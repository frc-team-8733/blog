---
title: 'Regionals day 1 [4/7/2022]'
date: '2022-04-07'
slug: 'regionals-day-1-4-7-2022'
category:
  - Uncategorized
---
Today is the first of three days at the Huntsville City Regionals. We got our pit setup yesterday. Today we did some finishing touches on the robot. We will be updating this post throughout the day.
# Pits

We got to see some cool pits. Here are some pictures.

![](http://web.archive.org/web/20220604173741im_/https://wp.arsrobotics.org/wp-content/uploads/2022/04/20220407_085342-1024x472.jpg)

![](http://web.archive.org/web/20220604173741im_/https://wp.arsrobotics.org/wp-content/uploads/2022/04/20220407_085337-1024x472.jpg)

![](http://web.archive.org/web/20220604173741im_/https://wp.arsrobotics.org/wp-content/uploads/2022/04/20220407_085243-944x2048.jpg)

![](http://web.archive.org/web/20220604173741im_/https://wp.arsrobotics.org/wp-content/uploads/2022/04/20220407_085238-944x2048.jpg)

# Visits

We also had some teams come visit us.

(*We are unable to recover this image for some reason*).

# Team pictures

We got quite a few pictures of our team.
![](http://web.archive.org/web/20220503132133im_/https://wp.arsrobotics.org/wp-content/uploads/2022/04/20220407_094700-768x354.jpg)

![](http://web.archive.org/web/20220503132133im_/https://wp.arsrobotics.org/wp-content/uploads/2022/04/20220407_094657-768x354.jpg)

![](http://web.archive.org/web/20220503132133im_/https://wp.arsrobotics.org/wp-content/uploads/2022/04/20220407_093244-708x1536.jpg)

![](http://web.archive.org/web/20220503132133im_/https://wp.arsrobotics.org/wp-content/uploads/2022/04/20220407_093243-708x1536.jpg)

![](http://web.archive.org/web/20220503132133im_/https://wp.arsrobotics.org/wp-content/uploads/2022/04/20220407_093241-708x1536.jpg)
