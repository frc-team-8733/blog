---
title: "Status Update [4/6/2022]"
date: "2022-04-07"
slug: 'status-update-4-6-2022'
tags:
  - Blog
  - Build
  - Robot
  - Statu
category:
  - Status Updates
---
Here is our third status update, and possibly the last one for a while. Yesterday was our last build meeting, although we may do a bit more work before the practice matches and the competition Thursday-Saturday.

# What we got done

 * Programming. I think the code has been done for a bit, however, I think there has been a few improvements just making
   the code better overall.
 * Battery mount. For the longest time, we just sat the battery next to the robot when testing. We finally got it mounted.
 * Electronics mounting. Up until Monday, we had all of our electronics, except for motors, on or development board. The
   electronics we had to mount included our Robo Rio (our robot controller,) the power distribution hub, motor controllers,
   wireless radio, webcam, and limelight (computer vision controller for detecting/tracking balls, hub, hanger, etc).

# What we are working on still

I believe we have everything finished already. If anything, we have to finish mounting plugging everything together.

Thanks for reading. As always, we hoped you enjoy this. Follow us on Mastodon at @arsrobotics@mastodon.online and we will see you next time.
