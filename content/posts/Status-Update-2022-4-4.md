---
title: "Status Update [4/4/2022]"
date: "2022-04-05"
slug: 'status-update-4-4-2022'
tags:
  - Robot
  - Status
  - Update
category:
  - Status Updates
---
Here is our second status update. It has been a bit since I posted one because I haven’t been to a build meeting since I got the blog back up until today.

# What we got done

 * Mounted Shooter. The shooter is now on the robot. We currently do not have a reliable
   way of getting the shooter.
 * Fold up intake. As the intake reaches out beyond the robot perimeter, we had to work on
   a way to fold the intake up, but also to be able to lower it at the start of the match.
   This was mostly done already as we had built the intake with the ability to raise up and down easily.

# What we are working on still

 * Programming. I do not know the status on this so I will see if we can get Carson, our lead developer, to give us an update.
 * Cargo delivery. We are currently working on a way to reliably get the ball to the shooter, as well as being able to store the ball between pickup and launcher.

# What to get done still

  * Mount the electronics. We have not done ANY mounting of electronics, except for motors. Everything else is still strapped to the dev board.

Thanks for reading. As always, we hoped you enjoy this. Follow us on Mastodon at @arsrobotics@mastodon.online and we will see you next time.
